<?php if(isset($cart_messages) && count($cart_messages) > 0) { ?>
<?php foreach((array)$cart_messages as $cart_message) { ?>
<span class="cart_message"><?php echo esc_html( $cart_message ); ?></span>
<?php } ?>
<?php } ?>

<?php if(wpsc_cart_item_count() > 0): ?>
	<div class="shoppingcart">

		<?php printf( _n('%d item', '%d items', wpsc_cart_item_count(), 'wpsc'), wpsc_cart_item_count() ); ?>
		<?php _e('Subtotal:', 'wpsc'); ?> <?php echo wpsc_cart_total_widget( false, false ,false ); ?>
		<br />




		<a target="_parent" href="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" title="<?php esc_html_e('Checkout', 'wpsc'); ?>" class="gocheckout"><?php esc_html_e('Checkout >', 'wpsc'); ?></a>
		<form action="" method="post" class="wpsc_empty_the_cart">
			<input type="hidden" name="wpsc_ajax_action" value="empty_cart" />
			<a target="_parent" href="<?php echo esc_url( add_query_arg( 'wpsc_ajax_action', 'empty_cart', remove_query_arg( 'ajax' ) ) ); ?>" class="emptycart" title="<?php esc_html_e('Empty Your Cart', 'wpsc'); ?>"><?php esc_html_e('Clear cart', 'wpsc'); ?></a>
		</form>
	</div><!--close shoppingcart-->
<?php else: ?>
	<p class="empty">
		<?php _e('Your shopping cart is empty', 'wpsc'); ?><br />
		<a target="_parent" href="<?php echo esc_url( get_option( 'product_list_url' ) ); ?>" class="visitshop" title="<?php esc_html_e('Visit Shop', 'wpsc'); ?>"><?php esc_html_e('Visit the shop', 'wpsc'); ?></a>
	</p>
<?php endif; ?>

<?php
wpsc_google_checkout();


?>
