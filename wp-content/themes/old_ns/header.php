<!DOCTYPE html>
<html lang="sv">

	<head>
		<!-- META DATA -->
		
		<meta http-equiv="Content-Type"	content="text/html; charset=utf-8" />
		
	
		
		<!-- CSS -->
		
		<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" title="Default" />
		<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,700' rel='stylesheet' type='text/css'>

		<!-- Javascript -->
		<script type="text/javascript" src="src/js/events/Event.js"></script>
		<script type="text/javascript" src="src/js/Main.js"></script>

		<title><?php wp_title( '|', true, 'right' ); ?></title>

		<?php wp_head(); ?>
	</head>


	<body>
		<div id="page-wrapper">
			<!-- 				HEADER 			-->
			<header role="banner">
				<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logos/tree.png" alt="Native Swedes"></a></h1>
				<div id="header-right">
					<form>
						<label for="search-field">Sök</label>
						<input type="text" placeholder="Sök..." name="searchField" id="search-field">
						<input type="submit" value="GO" name="search" id="search-submit"> 
					</form>
					<div id="shopping-cart">
						<?php echo wpsc_shopping_cart(); ?>
					</div>
				</div>
				<nav>
					
					<ul>
						<?php wp_list_pages('depth=1&title_li='); ?>
					</ul>
					<ul id="right-nav">
							<li><a href="#">Street Hippie</a></li>
							<li class="nav-last"><a href="#">Om oss</a></li>
					</ul>					
				</nav>
			</header>

			<!-- 				MAIN CONTENT 			-->
			<section role="main" id="main-content">