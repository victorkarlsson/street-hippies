/**
* @ author 	Victor Karlsson
* @ email: "victor.karlssn@gmail.com", 
* Projektarbete 
*/

var Main = {


							
	/**
	 *	The "core" method of the class. Call this to start this class
	 */

	init : function(){

		var div = document.getElementById("big-image"); 		
		Event.addEventListener(div, "mouseover", Main.addInfoText);
		Event.addEventListener(div, "mouseout", Main.deleteInfoText); 	

	},

	addInfoText : function(){

		
		var para 			= document.createElement('p');
			para.innerHTML 	= "Klicka för att se fler bilder";

		var div 			= document.getElementById("product-image-info");
			div.appendChild(para);
	},

	deleteInfoText : function(){

		var div 			= document.getElementById("product-image-info");
		if (div.childNodes.length > 0){
			while (div.firstChild) {
			   div.removeChild(div.firstChild);
			}
		}
	},


	

}

/*
 *	On window load runs the main method for the class: init().
 */

Event.addEventListener(window, "load", Main.init);
