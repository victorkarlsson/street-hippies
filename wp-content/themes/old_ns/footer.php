<!-- 				FOOTER 				-->
			<div class="footer">
				<footer id="footer">
					<nav id="footer-nav">
						<ul>
							<li><a href="#" class="active-link">Alla Produkter</a></li>
							<li><a href="#">Tshirt / Linne</a></li>
							<li><a href="#">Sweatshirts / Hoods</a></li>
							<li><a href="#">Headwear</a></li>
						</ul>
						<ul id="footer-nav-bottom">
							<li><a href="#">Street Hippie</a></li>
							<li><a href="#">Om oss</a></li>	
						</ul>						
					</nav>
					<img src="<?php echo get_template_directory_uri(); ?>/images/logos/owl.png" alt="Street Hippie Owl">
					<nav id="misc-navigation">
						<ul>
							<li><a href="#">Kontakt</a></li>
							<li><a href="#">Support/info</a></li>
							<li><a href="#">Återförsäljare</a></li>
						</ul>
						<p>&copy; Native Swedes 2013</p>
					</nav>
				</footer>
			</div>
		</div>

	</body>

</html>